:PROPERTIES:
:ID:       cec627c3-5d81-44ed-9fb2-26742662d7cb
:mtime:    20210701200843
:ctime:    20210204162417
:END:
#+title: pragmatic explanations cannot lead to platonic conclusions
#+filetags: thesis EIA explanation

* Context

Talk with Guido and Ronnie, also listening to cite:Batterman2001 .

* Body

I have become more sympathetic to the idea that [[id:72b645ad-5de5-4f43-97dd-0d48dc6fdd7f][an explanation is whatever scientists call an explanation]]. HOwever, what strikes me as very wrong with this si that such a vague assumption or definition of explanation is used to conclude an extrordinarily strong position, namely the fact that mathematical objects are platonically real.

What I do not like about this is that from a quite reasonable sociological account of science we derive extremely ontological conclusions. I think some additional "bridge-principles" are needed to account for such a leap of faith, and can definitely not be assumed.
