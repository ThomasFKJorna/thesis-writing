:PROPERTIES:
:ID:       78ecef7c-f9d9-4384-b6dc-7a5031414fff
:mtime:    20210701195412
:ctime:    20210330125526
:END:
#+title: Symmetry arguments are more interesting than symmetry properties
#+filetags: symmetry

* It is important to distinguish between symmetry arguments and symmetry properties, and to mostly focus on the former

Symmetry properties are properties of translational, rotational, reflection, reversal etc. invariance which are attributed to certain phenomena or laws. When we say that the speed of light is invariant under a change of reference frame, we /attribute/ the property of reference frame invariance to the speed of light. We are making a claim, but it is the most basic kind of claim we can make about something related to symmetry: saying that something has a kind of symmetry.

Symmetry /arguments/ arise from deriving certain other claims from the claim that a certain law or phenomena exhibits a kind of symmetry property. These are things like cite:Shech2019 's claim that "anyons arise because of the fact that quantum hall states exhibit the braid group".

* Ref

cite:Brading2017
