:PROPERTIES:
:ID:       8cd2e3e1-f09d-4b79-96cc-2fbdbd7e7370
:mtime:    20210701200741
:ctime:    20210503090553
:END:
#+title: Finding a way to approximately calculate anyons
#+filetags: meta anyons

* Finding a way to approximately calculate anyons

For the next part of my thesis (as of 03-05-21) I am looking into how to approximately calculate the anyonic phase in the Fractional Quantum hall effect. For this, I want to look at explanations etc of the [[id:5441a486-c826-424d-a30a-e08b9208642b][Berry phase]] in the [[id:c1fb832a-c00d-48d5-8397-1cdd1f7617df][Aharonov Bohm Effect]], and I have a couple of differenent resoureces to do this.

** Phil phys

cite:Dougherty2020b, which responds to cite:Shech2015b  and cite:Earman2019

** Phys

Primarily cite:Tong2016. but also cite:Rao2001 and cite:Stern2008a
