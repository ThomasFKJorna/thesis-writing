:PROPERTIES:
:ID:       da5add9d-61c4-453c-88be-f2c1eb8246fb
:mtime:    20210701200738
:ctime:    20210505153756
:END:
#+title: Equivalence Classes are either disjoint or equal
#+filetags: theorem mathematics


* Equivalence classes are either disjoint or equal

[[id:66fc979f-2e21-4469-8518-88a4aa19be9a][Equivalence Class]]
[[id:6546f4b1-b038-4961-821a-7341495fae1a][Disjoint]]
