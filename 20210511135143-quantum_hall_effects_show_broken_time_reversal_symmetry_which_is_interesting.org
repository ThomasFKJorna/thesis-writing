:PROPERTIES:
:ID:       02b6ff5c-6cae-468c-960b-6067c494c9d6
:mtime:    20210701200433
:ctime:    20210511135143
:END:
#+title: Quantum Hall effects show broken time-reversal symmetry, which is interesting
#+filetags: anyons FQHE

* Quantum Hall effects can show broken time-reversal symmetries

This is a good reason to bring up why [[id:a420af4a-97bf-4b87-8ab4-5f180808e27a][QHE]] are interested beyond the people who are immediately interested in condensed matter physics.

This is claimed in cite:Cohen2019 445.
