:PROPERTIES:
:ID:       37bfaecb-92e6-4cd7-9735-2b6c940c0e03
:mtime:    20210701200824
:ctime:    20210309112810
:END:
#+title: Infinite idealizations resemble Sorites because both have discontinuous transitions
#+filetags: vagueness central thesis


* Context

My thesis heavily centers around two topics: [[id:4d1d4b14-fb53-41d2-bc0d-a1c197cc6df0][infinite idealizations]] and [[id:2bd50b77-d226-4853-8bf2-d0e69b570a53][vagueness]]/[[id:207301aa-718d-417a-b3c0-103264d7a88f][sorites paradox]]. I want to basically find an answer to the latter, and to do so I am looking at the former, and this is the reason why. I am stuck writing [[id:edfca3ee-eb93-46ff-8e10-25794e4cd67e][FQHE Intro to show Ronnie and Guido]]

* Infinite Idealizations are similar to Sorites paradoxes because both are mathematical/logical reasons for why common explanations of phenomena don't (immediately) work out

Vagueness/Sorites paradox is having a continuous or discrete transition from one concept/object/... to another, where at the one end a predicate is true and at the other a predicate is false, and there being equally good reasons for believing that neither predicate actually can change.

Example:
1. A man with 100000 hairs is not bald.
2. If a man with N hairs is not bald, a man with N-1 hairs is not bald.
   ...
3. A man with 0 hairs is not bald.

3 seems false, but 1 seems true, so to fix this we can negate 2.
1. A man with 100000 hairs is not bald.
2. \not 2
3. There is a point at which a man with N hairs is not bald but at N-1 is bald.

3 seems false again, this time illustrating that we do not believe we can't draw lines that well.

Now for Inf Ids.

1. We do not observe the FQHE normally because the configuration space of the electrons is 3D
2. If the configuration space is 3D at n thickness/magnetic field strength, then the configuration space is 3D at n/2 thickness/ 2n magnetic field strength.
...
3. We never observe the FQHE

Or, other way around again

1. 1.
2. \not 2
...
3. There is a point where n thickness/magnetic field strength does not yield the FQHE, but n/2 thickness/ 2n magnetic field strength does.

3 also seems objectionable here, as we don't believe this symmethy is this easily broken



* THE MAJOR SIMILARITY IS THAT FOR BOTH CASES WE DO NOT HAVE A MECHANISM FOR DETERMINING /WHEN/ THIS CHANGE WOULD OCCUR.

So if the vagueness paradox is a problem for baldness than it is also a problem for this.

Ah. But then I can't use this as a counter argument
