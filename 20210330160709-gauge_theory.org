:PROPERTIES:
:ID:       f11abbe3-da85-4f8c-8914-552f991a3b00
:mtime:    20210701200801
:ctime:    20210330160709
:END:
#+title: Gauge theory
#+filetags: definition physics

A gauge theory is any theory with a [[id:c18acba0-88cd-48c2-bfa7-755bb15bee31][Gauge symmetry]].
