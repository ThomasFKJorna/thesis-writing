:PROPERTIES:
:ID:       a5590aa3-2d53-45d4-b7a7-f28f8c32ecbc
:mtime:    20210701200220
:ctime:    20210701200220
:END:
#+title: Lie Group
#+filetags: definition

* Lie Group

A *Lie Group* is a [[id:0bbb9ce7-191f-4fde-8924-cad8a3886e2e][Group]] describing continuous rather than discrete symmetries, roughly meaning that the [[id:fe87fdb4-b86b-48db-8a2d-eb402b095ad2][Cardinality]] of the group is $\aleph_1$ i.e. it has uncountably many elements.

* Examples

The most basic example of such a group would be the rotations of a circle in the $2D$ plane.
