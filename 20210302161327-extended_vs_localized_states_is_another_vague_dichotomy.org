:PROPERTIES:
:ID:       582866bf-f015-4bae-b8ca-307450e77d49
:mtime:    20210701200825
:ctime:    20210302161327
:END:
#+title: extended vs localized states is another vague dichotomy
#+filetags: stump "false dichotomy" FQHE

* Context
In order to explain the integer quantum hall effect, it is claimed that [[id:d5074aea-3aab-4aa7-954c-7307e3907a17][impurities turn many quantum states from extended to localized]]. However, this requires another dichotomy to strongly hold, namely that of localized vs extended states, both of which have mutually exclusive properties: extended states conduct, while localized states do not.

This is fishy
