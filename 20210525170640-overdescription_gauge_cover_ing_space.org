:PROPERTIES:
:ID:       0f439030-4c44-4c62-88a1-eca8e027a3da
:mtime:    20210701200717
:ctime:    20210525170640
:END:
#+title: Underdescription = gauge = cover(ing space)
#+filetags: physics mathematics gauge philosophy

* Underdescription = gauge = cover(ing space)

What philosophers call "underdescription", physicists call [[id:fab92aca-8392-49e1-8c66-922fc37d77a1][gauge freedom]], and mathematicians (roughly) call a [[id:32d4f910-5305-4e22-886f-5f823172aca3][Cover]]: they all describe a larger space which maps onto a smaller space which we actually care about.

This is not completely fair, as underdescription is the most general, gauge is more specific and cover is super specific.

But it works: a theory which underdescribes phenomena can map onto several more specific theories, a theory with a gauge freedom map onto several specific theories, and a space can be a cover for several spaces.
