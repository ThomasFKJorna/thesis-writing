:PROPERTIES:
:ID:       59db0283-e476-4fc6-a687-fb3acd020f80
:ROAM_REFS: cite:Earman2019
:mtime:    20210701194950
:ctime:    20210701194950
:END:
#+TITLE: Earman2019: The role of idealizations in the AharonovBohm effect
#+ROAM_KEY:
#+FILETAGS: reference explanation anyons idealization abeffect


* The role of idealizations in the AharonovBohm effect
  :PROPERTIES:
  :Custom_ID: Earman2019
  :DOI:  http://dx.doi.org/10.1007/s11229-017-1522-9
  :AUTHOR: Earman, J.
  :NOTER_DOCUMENT:
  :NOTER_PAGE:
  :END:



* Summary



* Rough note space
