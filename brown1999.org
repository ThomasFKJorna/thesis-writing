:PROPERTIES:
:ID:       df324e3c-ffb8-4521-bc24-4fafd273fc3b
:ROAM_REFS: cite:Brown1999
:mtime:    20210701235323
:ctime:    20210701235323
:END:
#+TITLE: Brown1999: Remarks on identical particles in de Broglie-Bohm theory

#+filetags: guido anyons reference

- tags ::
- keywords :: anyon, fqhe, guido


* Interesting
They provide a reason for why to get rid of the points $\Delta$ using de Broglie Bohm theory.

* Remarks on identical particles in de Broglie-Bohm theory
  :PROPERTIES:
  :Custom_ID: Brown1999
  :URL:
  :AUTHOR: Brown, Sjoqvist, Bacciagaluppi
  :NOTER_DOCUMENT:
  :NOTER_PAGE:
  :END:

