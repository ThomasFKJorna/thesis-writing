:PROPERTIES:
:ID:       588f9022-aa97-4f1c-8117-48cc32569afa
:mtime:    20210911175644
:ctime:    20210309144912
:END:
#+title:Stuck in my thesis hub
#+filetags: meta thesis


This meta note is here in order to get me back on track when I forget what my thesis is about and what the hell I'm doing even.

* This is what you are interested in:

#+transclude: t
[[id:99de5040-7b83-4433-850a-6db2d16a629b][Transitions from continuity to the discrete are not possible]]


* These are good points you made you keep forgetting
#+transclude: t
[[id:37bfaecb-92e6-4cd7-9735-2b6c940c0e03][inf id are similar to sorites because both have disc trans]]

[[id:0f8d8982-4c57-4aca-8803-bc6d0eef3e42][The Discretization of Quantum States is the biggest discontinuity in physics]]


* This is good advice you or someone else gave you that is worth recapping

#+transclude: t
[[id:f5e4542f-0c24-4f50-bdfc-5e6d39ab8001][if you cant loop back to your introduction, make shit up in your conclusion]]

#+transclud_ t
[[id:0e9ad6f0-a085-4a02-a889-d3ac0ceb907b][writing sucks, that's why you should do it]]

#+transclude t
[[id:2d68e306-1028-4c93-89a6-479a2e82379d][a good note should be concise, future text oriented, and your own words]]

* How to get back into writing

[[id:2d54ce7d-5106-4873-a439-5a50edff1156][Test driven writing]]
