:PROPERTIES:
:ID:       94719003-30b8-4262-b7b9-1ff3c2595997
:mtime:    20210701200724
:ctime:    20210512160337
:END:
#+title: My main goal is to show how the Berry phase approximates topological phase
#+filetags: phase anyons thesis fqhe


* The main goal of my case study on FQHE is to show that the Berry phase can approximate the topological phase

In my chapter [[id:66aff1d3-bd45-45e5-9d31-d4292d76ae8e][Structure FQHE/anyon chapter]], I am talking a lot of bullshit, which makes me forget my main goal:
Show how to approximate the [[id:e6303312-f094-4dd9-9d74-568bd31a89ea][The configuration space explanation of anyons]] using the  [[id:af98c8b8-d6d1-4443-893a-0a516b3664d2][Geometric Phase]]/[[id:5441a486-c826-424d-a30a-e08b9208642b][Berry phase]].

By approximate I mean this in Norton's sense [[id:e5db852b-a680-4f56-8a2d-cba2f13aecb4][Idealization vs Approximation]]: show how  taking increasing or decreasing some property gets us closer and closer to the topological phase calculation, in a smooth fashion.

This can be done in two ways: show how to calculate the phase, or show that the space is approximately simply connected. The latter is much more difficult.

* Entry points

As I think about it now, I need to figure out how [[id:981201fc-c29a-4120-bf30-105110cd4a29][Monodromy is an infinite idealization of holonomy]]
