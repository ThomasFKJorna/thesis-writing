:PROPERTIES:
:ID:       f257174f-e4a0-42f4-b455-0e2ba307c301
:mtime:    20210701195245
:ctime:    20210416113423
:END:
#+title: Complement
#+filetags: definition

* Complement of a set

Given a set $X$, we can define the *complement* of the subset $A \subseteq X$, denoted $A^c$, as
\begin{equation}
A^c = {x \in X | x \notin A}
    \label{eq:complement}
\end{equation}
all the points in the set $X$ that are not in $A$.

If $A=X$, then $A^c=\emptyset$
