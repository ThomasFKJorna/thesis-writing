:PROPERTIES:
:ID:       d23a01a0-a889-4412-8c3e-39142401a050
:mtime:    20210701195434
:ctime:    20210312130942
:END:
#+title: Is "Braids are not Getting Untangled" a Good Enough FQHE Explanation?
#+filetags: central FQHE

* Is the fact that anyonic worldlines /do/ not disentangle, as opposed to "not being able to disentangle" (in 2D) good enough to explain anyons?

cite:Shech2019 claims that
