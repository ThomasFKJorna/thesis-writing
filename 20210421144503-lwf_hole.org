:PROPERTIES:
:ID:       6221cc53-b4ae-46a6-8988-daf3830a628e
:mtime:    20210701195234
:ctime:    20210421144503
:END:
#+title: LWF hole
#+filetags: FQHE

* Hole version of LWF

If we excite the LWF with a hole (by threading a flux through it for instance) we get the wavefunction

\begin{equation}
\Psi_{hole}(z, \eta)=\prod_{i=1}(z_i-\eta)\prod_{k<i}(z_k-z_l)^n e^{-\sum_{i=1}^n |z_i|^2/{4l_B^2}}
    \label{eq:holeLWF}
\end{equation}
