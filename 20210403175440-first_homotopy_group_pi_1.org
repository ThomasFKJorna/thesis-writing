:PROPERTIES:
:ID:       803d6df6-3d4c-4471-b3bd-a022fad8d1ee
:mtime:    20210701200758
:ctime:    20210403175440
:END:
#+title: First homotopy group pi_1
#+filetags: topology definition

* First homotopy group

The first homotopy group $\pi_1$ of a configuration space is the [[id:0bbb9ce7-191f-4fde-8924-cad8a3886e2e][Group]] formed by inequivalent paths passing through a  given point in the configuration space.
The elements are the groups are paths (or more precisely, their traversal), with multiplication being defined as traversing two paths succesively, and the inverse being the traversal of the path backwards (identity being the point path).

