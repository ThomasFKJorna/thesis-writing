:PROPERTIES:
:ID:       b450a866-ad0d-4452-bb83-e7e169787953
:mtime:    20210701200747
:ctime:    20210416114640
:END:
#+title: Closure (set)
#+filetags: "functional analysis" topology definition

* Closure (set)

The *closure* $\bar{A}$ of a subset $A$ of a [[id:e997346e-b3dd-48da-8597-93acac415fa1][Metric Space]] $(X,d)$ is the smallest [[id:2a8826d4-52ec-40da-85c7-76d3deda2ee7][Closed set]] containing $A$, which is the union of $A$ and its [[id:d4e7ecd5-3f71-4e10-b967-b9ad290c27e5][Boundary]].
\begin{equation}
\bar{A}:=A\cup\partial A
    \label{eq:closure}
\end{equation}

Example: The closure of $(0,1)\in \mathbb{R}  $ is $[0,1]$
