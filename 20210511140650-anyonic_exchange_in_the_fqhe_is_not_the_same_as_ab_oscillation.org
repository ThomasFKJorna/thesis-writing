:PROPERTIES:
:ID:       0851b27e-f96a-4c37-836b-2e867c6e6774
:mtime:    20210701200725
:ctime:    20210511140650
:END:
#+title: Anyonic exchange in the FQHE is not the same as AB oscillation
#+filetags: AB FQHE anyons

* Anyonic exchange in the FQHE is not the same as AB oscillation

I thought that maybe I could explain the occurence of [[id:31cd49eb-770f-4d11-98b2-ca666b534201][anyons]] in [[id:2fa90488-38aa-478f-bc3a-2290fb9def10][the Fractional quantum hall effect (FQHE)]] by noting that they are the same as what happens in the  [[id:429093a9-7c54-4691-ae5f-949a311f5118][the Aharonov Bohm effect (AB)]], but this turns out not to be the case I see, as cite:Cohen2019 456 mentions that this can occur at the same time as exchange. Need to follow up on this.
