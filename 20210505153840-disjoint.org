:PROPERTIES:
:ID:       6546f4b1-b038-4961-821a-7341495fae1a
:mtime:    20210701200738
:ctime:    20210505153840
:END:
#+title: Disjoint
#+filetags: mathematics definition

* Disjoint

Two [[id:25a5762d-db61-4f84-9962-b96e20c7e84b][Set]]s are *disjoint* if they share no elements, i.e. their [[id:9199d16c-6f46-482c-a188-ce38d680775d][Intersection]] is the [[id:a5805690-630c-4146-a677-ce8b3ba12700][Empty set]].

 \[A\cap A'=\emptyset\]
