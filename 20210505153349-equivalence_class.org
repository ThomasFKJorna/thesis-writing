:PROPERTIES:
:ID:       66fc979f-2e21-4469-8518-88a4aa19be9a
:mtime:    20210701200739
:ctime:    20210505153349
:END:
#+title: Equivalence Class
#+filetags: definition mathematics

* Equivalence Class

** Munkres

Given an [[id:8136709d-9d43-44f6-8904-d411973bcfdd][Equivalence Relation]] $\~$ on a set A and an element $x\in A$, an *Equivalence Class* is a set $E\subseteq A$ determed by $x$ such that
\[E=\{y|y\~x\}\]

i.e. all the elements which stand in an equivalence relation to $x$.
