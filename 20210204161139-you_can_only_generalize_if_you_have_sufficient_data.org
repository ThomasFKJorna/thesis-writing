:PROPERTIES:
:ID:       d06eff85-7065-41af-b0e4-bb0720f7d4ec
:mtime:    20210701200844
:ctime:    20210204161139
:END:
#+title: you can only generalize if you have sufficient data
#+filetags: writing explanation thesis

* Context

Talk with Ronnie and Guido about my thesis. I was unsure how and when to tackle the topic of explanation in my thesis, as I want to be able to say something about it but can only say something if I've done the work.

* Body

It's easy to worry too much about where to place a certain topic in your thesis, and it's even easier to worry about how to tackle topics in turn. In general, I want to /know/ what I want to find before finding it, and basically just "check" whether I did the right thing.
This is not how things work. While you need a little bit of knowledge before going in, you also need to start going through the data as fast as possible, because the answers are in the doing, not the thinking beforehand, [[id:46d1e907-798c-474e-b36d-e41df8a0a5f4][eerst schrijven dan denken]].

