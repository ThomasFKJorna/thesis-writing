:PROPERTIES:
:ID:       90b942e4-ed45-4fd2-909f-e74312c2dab0
:mtime:    20210701200727
:ctime:    20210511132817
:END:
#+title: Curl
#+filetags: vectors definition mathematics

* Curl

The *curl*  $\nabla \times\mathbf{x}$ of a vector $\mathbf{x}$ (or more likely a [[id:4ab6fa70-e092-4c76-a6ca-33ef9d7493e8][Vector space]]) is the [[id:1aad6677-d406-41b6-aecf-be8c0b8e099f][Cross-product]] of the vector (vector) with the [[id:8aeed50e-4ba6-46f3-b1d2-4fdd0557951f][Nabla operator]]. In a crude way it  represents the amount of "rotation" going on in a certain vector space.
