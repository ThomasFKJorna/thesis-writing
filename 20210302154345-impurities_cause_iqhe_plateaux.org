:PROPERTIES:
:ID:       464e340d-b172-43d3-9b45-0f73c0551c25
:mtime:    20210701200827
:ctime:    20210302154345
:END:
#+title: impurities cause IQHE plateaux
#+filetags: FQHE

* Context

In [[id:c936a9ab-092e-4fe3-8b96-b6f4ae91e583][the Integer quantum hall effect (IQHE)]], plateaux in the transversal resistance are observed at integer multiples of the [[id:b9c034ae-c43c-488b-8c96-996789ac2695][filling factor]], the number of particles in the Landau level over the total space in the [[id:bce603cd-3eea-4dbc-b188-153845a24e16][Landau levels]]. While [[id:573dfbd0-2b6c-43b2-a08c-37f174de35c3][edge modes determine the values of the IQHE plateaux]], they do not explain why these plateaux persist over a range of magnetic fields.

* Body

The short explanation goes as follows:

#+transclude: t
[[id:5ef0b428-994c-4b5b-bf8f-c8b2491baf24][impurities break the degeneracy of the landau levels]]


#+transclude: t
[[id:d5074aea-3aab-4aa7-954c-7307e3907a17][impurities turn many quantum states from extended to localized]]


Then, when all the extended states get filled and the magnetic field gets turned down, there is less space in the Landau level for the extended state. However, instead of jumping to the next extended state, they jump to the localized ones. These localized states do not conduct and thus do not change the conductivity.

The weird thing is that one would expect the resistivity to go down anyway, since the Landau level still holds fewer states. Strangely enough however
#+transclude: t
[[id:a133889b-5982-43d1-a4c5-e46312369b52][extended states conduct more when less]]
